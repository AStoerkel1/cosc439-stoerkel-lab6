#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <stdlib.h>

sem_t eat;
sem_t call;

int slices = 0;
int num_of_pizzas = 1;

void pizza_consumer(long student){
	sem_wait(&eat);
	printf("student %ld eats slice %d from pizza #%d \n", student, ++slices, num_of_pizzas);
	sem_post(&eat);
	sleep(rand() %3);
}
void pizza_caller(long student){
	if(num_of_pizzas == 13){
		num_of_pizzas++;
		sem_post(&call);
		return;
	}
	sem_wait(&eat);
	printf("student %ld orders pizza #%d\n", student, ++num_of_pizzas);
	sleep(rand()%3);
	printf("pizza #%d arrives\n", num_of_pizzas);
	slices = 0;
	sem_post(&call);//call to sem_trywait(&call) in conditional of study_group()
	sem_post(&eat);
}

void *study_group(void * arg){
	long student = (long)arg;
	//students should stop ordering but keep eating after the 13th pizza is ordered
	//to make num_of_pizzas work as the sentinel value I increment it without printing after the last
	// piece of the 13th pizza is eaten. see: pizza_caller()
	while(num_of_pizzas <= 13){
		if(slices < 8){pizza_consumer(student);}
		//sem_trywait() doesnt block if it can't immediately decrement
		//so the thread will move to else and sleep until the next pizza arrives
		//or num_of_pizzas goes above 13
		else if(sem_trywait(&call) == 0){pizza_caller(student);}
		else{while(slices >= 8 && num_of_pizzas <=13){sleep(1);}}
	}
}

void main(int c, char *argv[]) {
	int students = atoi(argv[1]);
	if(students > 5 || students < 2){
		fprintf(stderr, "invalid number of students [2-5]\n");
		return;
	}
	sem_init(&eat, 0, 1);
	sem_init(&call, 0, 1);
	pthread_t tids[students];
	for (long unsigned int i = 0; i<students; i++){
		tids[i] = i+1;
		pthread_attr_t attr;
		pthread_attr_init(&attr);
		pthread_create(&tids[i], &attr, study_group, (void *)tids[i]);
	}
	for(int i = 0; i<students; i++){
		pthread_join(tids[i], NULL);
	}
	sem_destroy(&call);
	sem_destroy(&eat);
}
