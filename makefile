all: votes pizza

pizza:pizzastudy.c
	gcc -pthread -o pizza pizzastudy.c

votes:votecountersem.c
	gcc -pthread -o votes votecountersem.c

run: votes pizza
	./votes
	./pizza 5
clean:
	rm votes pizza
